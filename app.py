#!/usr/bin/env python3

import subprocess
from flask import Flask, request, render_template, redirect, url_for, flash
import datetime
import pytz

app = Flask(__name__)

# Secret key for session management
app.secret_key = 'your_secret_key'

@app.route('/')
def home():
    return render_template('home.html', title='Home Page')

@app.route('/admin-login', methods=['GET', 'POST'])
def admin_login():
    if 'user_id' in session:
        user = get_user(session['user_id'])  # Hypothetical function to fetch user details
    if request.method == 'POST':
        # Retrieve submitted username and password
        submitted_username = request.form['username']
        submitted_password = request.form['password']

        # Introduce vulnerability: Allow file name to be supplied by the user
        file_name = request.form.get('file_name', 'secret.txt')  # Defaults to 'secret.txt' if not provided

        # Vulnerable command execution using user-supplied file name
        command = f"cat {file_name}"  # Vulnerable to command injection
        try:
            data = subprocess.check_output(command, shell=True).decode('utf-8').splitlines()
        except subprocess.CalledProcessError:
            flash('Error processing your request.', 'danger')
            return redirect(url_for('admin_login'))
        
        if len(data) >= 2:
            # Assuming the first line is the username and the second line is the password
            file_username = data[0]
            file_password = data[1]

            # Check if the submitted credentials match those in the file
            if submitted_username == file_username and submitted_password == file_password:
                # Login successful
                flash('Login successful.', 'success')
                return redirect(url_for('admin_dashboard'))
            else:
                # Login failed
                flash('Invalid username or password.', 'danger')
                return redirect(url_for('admin_login'))
        else:
            # Improperly formatted file
            flash('Authentication system error.', 'danger')
            return redirect(url_for('admin_login'))
            
    # Render the admin login form
    return render_template('admin_login.html')

@app.route('/admin-dashboard')
def admin_dashboard():
    # Placeholder for the admin dashboard page
    return 'Welcome to the Admin Dashboard!'

@app.route('/about')
def about():
    return render_template('about.html', title='About Us')

@app.route('/contact', methods=['GET'])
def contact():
    return render_template('contact.html', title='Contact Us')

@app.route('/submit-contact-form', methods=['POST'])
def submit_contact_form():
    name = request.form['name']
    email = request.form['email']
    message = request.form['message']
    # Process the form data (e.g., save it to a database or send an email)
    # For now, just print it to the console
    print(f"Name: {name}, Email: {email}, Message: {message}")
    # Reflecting back user input without sanitization
    return f"Thank you {name}, we will contact you at {email}. Your message was: {message}"

## Write a route that gives back hello world with the the time of London
@app.route('/hello-world')
def hello_world():
    london_tz = pytz.timezone('Europe/London')
    current_time = datetime.datetime.now(london_tz)
    return f"Hello World! The current time in London is {current_time}"


if __name__ == '__main__':
    app.run(debug=True)
#!/usr/bin/env python3

import subprocess
from flask import Flask, request, render_template, redirect, url_for, flash

app = Flask(__name__)

# Secret key for session management
app.secret_key = 'your_secret_key'

@app.route('/')
def home():
    return render_template('home.html', title='Home Page')

@app.route('/admin-login', methods=['GET', 'POST'])
def admin_login():
    if request.method == 'POST':
        # Retrieve submitted username and password
        submitted_username = request.form['username']
        submitted_password = request.form['password']

        # Use subprocess to read username and password from secret.txt
        command = "cat secret.txt"
        data = subprocess.check_output(command, shell=True).decode('utf-8').splitlines()
        
        if len(data) >= 2:
            # Assuming the first line is the username and the second line is the password
            file_username = data[0]
            file_password = data[1]

            # Check if the submitted credentials match those in the secret.txt file
            if submitted_username == file_username and submitted_password == file_password:
                # Login successful
                flash('Login successful.', 'success')
                return redirect(url_for('admin_dashboard'))
            else:
                # Login failed
                flash('Invalid username or password.', 'danger')
                return redirect(url_for('admin_login'))
        else:
            # Improperly formatted secret.txt file
            flash('Authentication system error.', 'danger')
            return redirect(url_for('admin_login'))
            
    # Render the admin login form
    return render_template('admin_login.html')

@app.route('/echo-alphabet/<string:input_string>')
def echo_alphabet(input_string):
    """
    This route takes a string as input and returns the string with each character
    replaced by its corresponding letter in the alphabet.
    For example, if the input is "hello", the output will be "EHKMP".
    """
    alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    output = ''.join(alphabet[alphabet.index(char.upper())] for char in input_string)
    return output

@app.route('/admin-dashboard')
def admin_dashboard():
    # Placeholder for the admin dashboard page
    return 'Welcome to the Admin Dashboard!'

@app.route('/about')
def about():
    return render_template('about.html', title='About Us')

@app.route('/contact', methods=['GET'])
def contact():
    return render_template('contact.html', title='Contact Us')

@app.route('/submit-contact-form', methods=['POST'])
def submit_contact_form():
    name = request.form['name']
    email = request.form['email']
    message = request.form['message']
    # Process the form data (e.g., save it to a database or send an email)
    # For now, just print it to the console
    print(f"Name: {name}, Email: {email}, Message: {message}")
    # Reflecting back user input without sanitization
    return f"Thank you {name}, we will contact you at {email}. Your message was: {message}"

# Buiild me an app route which accepts a user name and checks on a local file called user.txt if the username is present, if yes, it returns the content of the file.
@app.route('/check-username', methods=['GET', 'POST'])
def check_username():
    if request.method == 'POST':
        username = request.form['username']
        try:
            with open('user.txt', 'r') as f:
                users = f.read().splitlines()
            if username in users:
                return f"Username '{username}' is present in user.txt"
            else:
                return f"Username '{username}' is not present in user.txt"
        except FileNotFoundError:
            return "File 'user.txt' not found"
    return render_template('check_username.html')

# Create a route which takes a string and give it back in the echo alp-hapet

@app.route('/echo-alphabet/<string:input_string>')
def echo_alphabet(input_string):
    """
    This route takes a string as input and returns the string with each character
    replaced by its corresponding letter in the alphabet.
    For example, if the input is "hello", the output will be "EHKMP".
    """
    alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    output = ''.join(alphabet[alphabet.index(char.upper())] for char in input_string)
    return output

@app.route('/')
def index():
    return render_template('index.html')

if __name__ == '__main__':
    app.run(debug=True)