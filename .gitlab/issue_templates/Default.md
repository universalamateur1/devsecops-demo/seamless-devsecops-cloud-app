<!-- This issue template can be used as a great starting point for feature requests. Learn more about the process: https://handbook.gitlab.com/handbook/product/how-to-engage/#customer-feature-requests.  
The goal of this template is brevity for quick/smaller iterations. For a more thorough list of considerations for larger features or feature sets, you can leverage the detailed [feature proposal](https://gitlab.com/gitlab-org/gitlab/-/blob/master/.gitlab/issue_templates/Feature%20proposal%20-%20detailed.md). -->
# Feature Request

## Problem to solve

<!-- What is the user problem you are trying to solve with this issue? -->

## Proposal

<!-- Use this section to explain the feature and how it will work. It can be helpful to add technical details, design proposals, and links to related epics or issues. -->

## Intended users

<!-- Who will use this feature? If known, include any of the following: types of users (e.g. Developer), personas, or specific company roles (e.g. Release Manager). It's okay to write "Unknown" and fill this field in later.
-->

## Tasks and activities

* [ ] Task 1
* [ ] Task 2

## Acceptance criteria

* [ ] Definition of Done
* [ ] Iterated

/label ~"workflow::design"
/due in 14 days
/assign @fsieverding
/label ~"type::feature"
