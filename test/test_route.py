import pytest
from app import app

@pytest.fixture
def client():
    with app.test_client() as client:
        yield client

def test_home_page(client):
    rv = client.get('/')
    assert rv.status_code == 200
    assert b'Home Page' in rv.data

def test_about_page(client):
    rv = client.get('/about')
    assert rv.status_code == 200
    assert b'About Us' in rv.data

def test_contact_page(client):
    rv = client.get('/contact')
    assert rv.status_code == 200
    assert b'Contact Us' in rv.data

#def test_hello_world(client):
#    rv = client.get('/hello-world')
#    assert rv.status_code == 200
#    assert b'Hello World!' in rv.data

def test_admin_login_page(client):
    rv = client.get('/admin-login')
    assert rv.status_code == 200
    assert b'Admin Login' in rv.data

def test_admin_dashboard_redirect(client):
    rv = client.post('/admin-login', data=dict(username='admin', password='password'), follow_redirects=True)
    assert b'Welcome to the Admin Dashboard!' not in rv.data  # Assuming credentials are not in secret.txt

def test_submit_contact_form(client):
    rv = client.post('/submit-contact-form', data=dict(name='Test User', email='test@example.com', message='Hello'), follow_redirects=True)
    assert rv.status_code == 200
    assert b'Thank you Test User' in rv.data

def test_check_username_present(client, monkeypatch):
    def mock_open(*args, **kwargs):
        from io import StringIO
        return StringIO('testuser\nanotheruser')

    monkeypatch.setattr('builtins.open', mock_open)
    rv = client.post('/check-username', data=dict(username='testuser'), follow_redirects=True)
    assert b"Username 'testuser' is present in user.txt" in rv.data

def test_check_username_not_present(client, monkeypatch):
    def mock_open(*args, **kwargs):
        from io import StringIO
        return StringIO('testuser\nanotheruser')

    monkeypatch.setattr('builtins.open', mock_open)
    rv = client.post('/check-username', data=dict(username='missinguser'), follow_redirects=True)
    assert b"Username 'missinguser' is not present in user.txt" in rv.data

def test_check_username_file_not_found(client, monkeypatch):
    def mock_open(*args, **kwargs):
        raise FileNotFoundError

    monkeypatch.setattr('builtins.open', mock_open)
    rv = client.post('/check-username', data=dict(username='testuser'), follow_redirects=True)
    assert b"File 'user.txt' not found" in rv.data
