# Seamless DevSecOps - Building and Deploying a Cloud-Native Application with GitLab

## Description

This project demonstrates the end-to-end lifecycle of a cloud-native application utilizing GitLab's integrated DevOps platform. It showcases how seamless collaboration between development, security, and operations leads to faster, more secure software delivery. Through this demo, we explore the use of GitLab CI/CD pipelines for automated testing, building, and deploying, integrated with comprehensive security scanning to ensure code quality and security compliance. Participants will gain insights into leveraging GitLab for project planning, source code management, continuous integration (CI), continuous deployment (CD), and monitoring within a DevSecOps framework. Our goal is to highlight the efficiency and effectiveness of GitLab in facilitating a secure, agile software development process for cloud-native applications.

## Installation _Optional_ Commands for local installation and prep

1. Git Clone this repo
1. virtual enviroment `python3 -m venv ./.venv`
1. Install reqs `./.venv/bin/python3 -m  pip install -r requirements.txt`
1. run app `./.venv/bin/python3 -m flask run`
1. if new reqs come through, install in venv, then `./.venv/bin/python3 -m pip freeze >| ./requirements.txt`

For local AI help run `ollama run codellama:34b-python-q6_K` or `ollama run codestral`

## Authors and acknowledgment

1. Falko Sieverding - @fsiverding (GitLab Account)
2. Falko Sieverding - @UniversalAmateur (Private Account)

## License

[MIT License](LICENSE)

## Project status

WIP
