# Demo Run Book - Dev in GitLab

## Prep Work

- [ ] Have Merge request/issue Default template in the Project
- [ ] Have Commit templates Ready to go

Example [https://gitlab.com/-/project/17735787/uploads/0efd43217a0ca2d02ff6f3fffeeddd2a/video3839078639.mp4

## The Demo of a Developer working in Code

### 1. Starting with an Issue

1. Have an issue for a new functionality assigned to the demo person
2. Have a conversation around the issue in it
3. Use the Summarize this issue functionality

### 2. Create a Merge Request

1. Open a Merge Request out of the issue
2. FIll the Template with assigned issues details
3. Open The Merge Request out of the issue

### 3. Coding in the Merge Request

1. Opene the MR in the WebIDE
2. Generate with Code Suggestion the working code over a prompt in the Code
3. Mark the new code and use GitLab Duo Chat to let it be explained to you as Documentation
4. Use the /test command to create test for your new function
5. Use the /Refactor command for a complicated function
6. Push the Commits in the Merge request

### 4. Working in The MR

1. View the Pipeline just started
2. Switch to an old Pipeline with a failed Job
3. Show Root Cause Analysis
4. Jump in The Merge Request and show Summarize Merge Request Function
5. Show Code Review Functionality

### 4. Vulnerability Management

1. Jump into the Vulnerability Report
2. Show explain this Vulnerbility
3. Use remidiate the Vulnerability on a SAST easy vuln
